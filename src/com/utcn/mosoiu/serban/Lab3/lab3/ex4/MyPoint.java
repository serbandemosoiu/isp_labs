package niga.cristina.lab3.ex4;

public class MyPoint {
    int x;
    int y;
    public MyPoint(){
        x=0;
        y=0;
    }
    public MyPoint(int x,int y){
        this.x=x;
        this.y=y;
    }
    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public void setX(int x){
        this.x=x;
  }
  public void setY(int y){
        this.y=y;
  }
  public void setXY(int x,int y){
        this.x=x;
        this.y=y;
  }
  public String toString(){
        return "("+this.x+","+this.y+")";
  }
  public double getDistance(int x,int y){
        return Math.sqrt(Math.pow((this.x-x),2)+Math.pow((this.y-y),2));
  }
    public double getDistance(MyPoint p){
        return Math.sqrt(Math.pow((this.x-p.x),2)+Math.pow((this.y-p.y),2));
    }





}

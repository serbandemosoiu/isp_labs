package niga.cristina.lab3.ex3;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;

    }
    public String getName(){
        return this.name;
    }
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email=email;
    }
    public char getGender(){
        return  this.gender;
    }
    public String toString(){
        return "Author-"+this.name+" ("+this.gender+") at email "+this.email;
    }
}
